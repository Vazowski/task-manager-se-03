package ru.iteco.taskmanager.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import ru.iteco.taskmanager.entity.ConsoleTemplates;
import ru.iteco.taskmanager.entity.Project;

public class ProjectController implements IController{

	private static List<Project> projectArray = new ArrayList<>();
	
	private static Scanner scanner = new Scanner(System.in);
	
	private int index;
	private String consoleInput;

	@Override
	public void create() {
		System.out.print("Name of project: ");
		consoleInput = scanner.nextLine();
		index = exist(consoleInput);
		if (index == -1) {
			System.out.print("Description of project: ");
			String description = scanner.nextLine();
			projectArray.add(new Project(consoleInput, description));
			ConsoleTemplates.isDone();
		} else {
			ConsoleTemplates.isExist("Project");
		}
	}

	@Override
	public void show() {
		System.out.print("Name of project that you want to show: ");
		consoleInput = scanner.nextLine();
		index = exist(consoleInput);
		if (index >= 0) {
			System.out.println(projectArray.get(index).toString());
		} else {
			ConsoleTemplates.noExist("Project");
		}
	}

	@Override
	public void showAll() {
		for (int i = 0; i < projectArray.size(); i++) {
			System.out.println("[" + (i + 1) + "]");
			System.out.println(projectArray.get(i).toString());
		}
		if (projectArray.size() == 0) {
			ConsoleTemplates.isEmpty();
		}
	}
	
	@Override
	public void update() {
		System.out.print("Name of project that you want to update: ");
		consoleInput = scanner.nextLine();
		index = exist(consoleInput);
		if (index >= 0) {
			System.out.print("What you want to update? [name/description]: ");
			consoleInput = scanner.nextLine();
			if ("name".equals(consoleInput) || "Name".equals(consoleInput)) {
				System.out.print("New name of project: ");
				consoleInput = scanner.nextLine();
				index = exist(consoleInput);
				if (index == -1) {
					projectArray.get(index).setName(consoleInput);
				} else {
					ConsoleTemplates.isExist("Project");
					return;
				}
			} 
			if ("description".equals(consoleInput) || "Description".equals(consoleInput)) {
				System.out.print("New description of project: ");
				consoleInput = scanner.nextLine();
				projectArray.get(index).setDescription(consoleInput);
			}
			ConsoleTemplates.isDone();
		} else {
			ConsoleTemplates.noExist("Project");
		}
	}

	@Override
	public void remove() {
		System.out.print("Name of project that you want to delete: ");
		consoleInput = scanner.nextLine();
		index = exist(consoleInput);
		if (index >= 0) {
			TaskController.removeByUuid(projectArray.get(index).getUuid());
			projectArray.remove(index);
			ConsoleTemplates.isDone();
		} else {
			ConsoleTemplates.noExist("Project");
		}
	}
	
	private int exist(String name) {
		for (int i = 0; i < projectArray.size(); i++) {
			if (projectArray.get(i).getName().equals(name)) {
				return i;
			}
		}
		return -1;
	}
	
	public static int getSize() {
		return projectArray.size();
	}
	
	public static String getNames() {
		String result = "";
		for (int i = 0; i < projectArray.size(); i++) {
			result += "[" + (i + 1) + "]  -  " + projectArray.get(i).getName() + "\n";
		}
		result += "> ";
		return result;
	}
	
	public static String getUuidById(int id) {
		return projectArray.get(id).getUuid();
	}
}
