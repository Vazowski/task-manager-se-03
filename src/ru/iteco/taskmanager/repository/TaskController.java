package ru.iteco.taskmanager.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import ru.iteco.taskmanager.entity.ConsoleTemplates;
import ru.iteco.taskmanager.entity.Task;

public class TaskController implements IController{

	private static List<Task> taskArray = new ArrayList<Task>();
	
	private static Scanner scanner = new Scanner(System.in);
	
	private int index;
	private int projectArraySize;
	private String consoleStringInput;
	private int consoleIntInput;
	
	@Override
	public void create() {
		if (!checkProjectsSize()) {
			return;
		}
		System.out.println("Number of the project to which we add the task: ");
		System.out.print(ProjectController.getNames());
		consoleIntInput = scanner.nextInt();
		if (consoleIntInput > 0 && consoleIntInput <= ProjectController.getSize()) {
			System.out.print("Name of task: ");
			consoleStringInput = scanner.nextLine();
			consoleStringInput = scanner.nextLine();
			index = exist(consoleIntInput - 1, consoleStringInput);
			if (index == -1) {
				System.out.print("Description of task: ");
				String description = scanner.nextLine();
				taskArray.add(new Task(consoleStringInput, description, ProjectController.getUuidById(consoleIntInput - 1)));
				ConsoleTemplates.isDone();
			} else {
				ConsoleTemplates.isExist("Task");
			}
		} else {
			ConsoleTemplates.noExist("Project");
		}
	}

	@Override
	public void show() {
		if (!checkProjectsSize()) {
			return;
		}
		System.out.println("What project does the task relate to: ");
		System.out.print(ProjectController.getNames());
		consoleIntInput = scanner.nextInt();
		if (consoleIntInput > 0 && consoleIntInput <= ProjectController.getSize()) {
			System.out.print("Name of task that you want to show: ");
			consoleStringInput = scanner.nextLine();
			consoleStringInput = scanner.nextLine();
			index = exist(consoleIntInput - 1, consoleStringInput);
			if (index >= 0) {
				System.out.println(taskArray.get(index).toString());
			} else {
				ConsoleTemplates.noExist("Task");
			}
		} else {
			ConsoleTemplates.noExist("Project");
		}
	}

	@Override
	public void showAll() {
		if (!checkProjectsSize()) {
			return;
		}
		int position = 1;
		System.out.println("What project does the task relate to: ");
		System.out.print(ProjectController.getNames());
		consoleIntInput = scanner.nextInt();
		if (consoleIntInput > 0 && consoleIntInput <= ProjectController.getSize()) {
			for (int i = 0; i < taskArray.size(); i++) {
				if (ProjectController.getUuidById(consoleIntInput - 1).equals(taskArray.get(i).getProjectUUID())) {
					System.out.println("[" + (position++) + "]");
					System.out.println(taskArray.get(i).toString());
				}
			}
			if (taskArray.size() == 0) {
				ConsoleTemplates.isEmpty();
			}		
		} else {
			ConsoleTemplates.noExist("Project");
		}
	}

	@Override
	public void update() {
		if (!checkProjectsSize()) {
			return;
		}		
		System.out.println("What project does the task relate to: ");
		System.out.print(ProjectController.getNames());
		consoleIntInput = scanner.nextInt();
		if (consoleIntInput > 0 && consoleIntInput <= ProjectController.getSize()) {
			System.out.print("Name of task that you want to update: ");
			consoleStringInput = scanner.nextLine();
			consoleStringInput = scanner.nextLine();
			index = exist(consoleIntInput - 1, consoleStringInput);
			if (index >= 0) {
				System.out.print("New name of task: ");
				String newName = scanner.nextLine();
				taskArray.get(index).setName(newName);
				ConsoleTemplates.isDone();
			} else {
				ConsoleTemplates.isExist("Task");
			}
		} else {
			ConsoleTemplates.noExist("Project");
		}
	}

	@Override
	public void remove() {
		if (!checkProjectsSize()) {
			return;
		}		
		System.out.println("What project does the task relate to: ");
		System.out.print(ProjectController.getNames());
		consoleIntInput = scanner.nextInt();
		if (consoleIntInput > 0 && consoleIntInput <= ProjectController.getSize()) {
			System.out.print("Name of task that you want to remove: ");
			consoleStringInput = scanner.nextLine();
			consoleStringInput = scanner.nextLine();
			index = exist(consoleIntInput - 1, consoleStringInput);
			if (index >= 0) {
				taskArray.remove(index);
				ConsoleTemplates.isDone();
			} else {
				ConsoleTemplates.isExist("Task");
			}
		} else {
			ConsoleTemplates.noExist("Project");
		}
	}

	public static void removeByUuid(String uuid) {
		for (int i = 0; i < taskArray.size(); i++) {
			if (uuid.equals(taskArray.get(i).getProjectUUID())) {
				taskArray.remove(i);
			}
		}
	}
	
	private boolean checkProjectsSize() {
		projectArraySize = ProjectController.getSize(); 
		if (projectArraySize < 1) {
			ConsoleTemplates.noExist("Project");
			return false;
		}
		return true;
	}
	
	private int exist(int projectNumber, String name) {
		String projectUuid = ProjectController.getUuidById(projectNumber);
		for (int i = 0; i < taskArray.size(); i++) {
			if (projectUuid.equals(taskArray.get(i).getProjectUUID()) && name.equals(taskArray.get(i).getName())) {
				return i;
			}
		}
		return -1;
	}
}
