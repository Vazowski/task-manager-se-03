package ru.iteco.taskmanager.repository;

public interface IController {

	void create();
	void show();
	void showAll();
	void update();
	void remove();
}
