package ru.iteco.taskmanager.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UUIDController {

	private static List<String> uuidArray = new ArrayList<>();
	
	public static String getUUID() {
		String uuid;
		do {
			uuid = UUID.randomUUID().toString();
			if (!uuidArray.contains(uuid)) {
				uuidArray.add(uuid);
			}
		} while(!uuidArray.contains(uuid));
		return uuid;
	}
}
