package ru.iteco.taskmanager.repository;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateController {

	private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
	
	public static String getDateBegin() {
		return dateFormat.format(new Date());
	}
}
