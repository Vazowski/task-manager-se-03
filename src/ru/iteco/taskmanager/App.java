package ru.iteco.taskmanager;

import java.util.Scanner;

import ru.iteco.taskmanager.commands.Commands;
import ru.iteco.taskmanager.repository.ProjectController;
import ru.iteco.taskmanager.repository.TaskController;
import ru.iteco.taskmanager.service.Bootstrap;

public class App
{
	private static Scanner scanner;
    private static String userInput;
	
    public static ProjectController projectController = new ProjectController();
    public static TaskController taskController = new TaskController();
    
    public static void main( String[] args )
    {
    	System.out.println("Enter command (type help for more information)");
    	init();
    	
    	while(true) {
    		System.out.print("> ");    		
    		scanner = new Scanner(System.in);
    		userInput = scanner.nextLine();
    		if (Commands.keyExist(userInput)) {
    			Bootstrap.execute(userInput);
    		}
    	}
    }
    
    private static void init() {
    	Commands.makeDescription();
    }
}
