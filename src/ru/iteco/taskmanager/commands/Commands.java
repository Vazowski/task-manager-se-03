package ru.iteco.taskmanager.commands;

import java.util.LinkedHashMap;
import java.util.Map;

public class Commands {

	private static Map<String, String> description;
	
	public static void makeDescription() {
		description = new LinkedHashMap<String, String>();
		
		description.put("help", "  -  show help information");
		description.put("exit", "  -  exit the programm");
    	
		description.put("cp", "  -  create new project");
		description.put("rp", "  -  show project");
		description.put("allp", "  -  show all projects");
		description.put("up", "  -  update project");
		description.put("dp", "  -  delete project");
    	
		description.put("ct", "  -  create new task");
		description.put("rt", "  -  show task");
		description.put("allt", "  -  show all task");
		description.put("ut", "  -  update task");
		description.put("dt", "  -  delete task");
    }
	
	public static void showAll() {
		System.out.println();
		for (String key : description.keySet()) {
			if (!key.equals("help"))
			System.out.println(key + description.get(key));
		}
	}
	
	public static boolean keyExist(String _key) {
		for (String key : description.keySet()) {
			if (key.equals(_key))
				return true;
		}
		System.out.println("Command doesn't exist");
		return false;
	}
}
