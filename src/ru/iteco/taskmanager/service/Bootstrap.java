package ru.iteco.taskmanager.service;

import ru.iteco.taskmanager.App;
import ru.iteco.taskmanager.commands.Commands;
import ru.iteco.taskmanager.entity.Literas;

public class Bootstrap {

	public static void execute(String command) {
		switch (command) {
		case Literas.EXIT:
			System.exit(0);
		case Literas.HELP:
			Commands.showAll();
			break;
		case Literas.NEW_PROJECT:
			App.projectController.create();
			break;
		case Literas.SHOW_PROJECT:
			App.projectController.show();
			break;
		case Literas.SHOW_ALL_PROJECT:
			App.projectController.showAll();
			break;
		case Literas.UPDATE_PROJECT:
			App.projectController.update();
			break;
		case Literas.DELETE_PROJECT:
			App.projectController.remove();
			break;
		case Literas.NEW_TASK:
			App.taskController.create();
			break;
		case Literas.SHOW_TASK:
			App.taskController.show();
			break;
		case Literas.SHOW_ALL_TASK:
			App.taskController.showAll();
			break;
		case Literas.UPDATE_TASK:
			App.taskController.update();
			break;
		case Literas.DELETE_TASK:
			App.taskController.remove();
			break;
		}
	}
	
}
