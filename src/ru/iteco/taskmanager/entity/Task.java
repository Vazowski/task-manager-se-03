package ru.iteco.taskmanager.entity;

import ru.iteco.taskmanager.repository.DateController;
import ru.iteco.taskmanager.repository.UUIDController;

public class Task {

	private String uuid;
	private String projectUUID;
    private String name;
    private String description;
    private String dateBegin;
    private String dateEnd;
    
    public Task() {
    	
    }
    
    public Task(String _name, String _description, String _projectUuid) {
    	this.uuid = UUIDController.getUUID();
    	this.projectUUID = _projectUuid;
    	this.name = _name;
    	this.description = _description;
    	this.dateBegin = DateController.getDateBegin();
    	this.dateEnd = DateController.getDateBegin();
    }
    
	public String getTaskUUID() {
		return uuid;
	}
	
	public void setTaskUUID(String taskUUID) {
		this.uuid = taskUUID;
	}
	
	public String getProjectUUID() {
		return projectUUID;
	}
	
	public void setProjectUUID(String projectUUID) {
		this.projectUUID = projectUUID;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDateBegin() {
		return dateBegin;
	}
	
	public void setDateBegin(String dateBegin) {
		this.dateBegin = dateBegin;
	}
	
	public String getDateEnd() {
		return dateEnd;
	}
	
	public void setDateEnd(String dateEnd) {
		this.dateEnd = dateEnd;
	}
	
	@Override
	public String toString() {
		return "Name: " + this.name + "\nDescription: " + this.description + "\nDateBegin: " + this.dateBegin + "\nDateEnd: " + this.dateEnd + "\nUUID: " + this.uuid + "\nprojectUUID: " + this.projectUUID;
	}
}
