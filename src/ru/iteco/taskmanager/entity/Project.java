package ru.iteco.taskmanager.entity;

import ru.iteco.taskmanager.repository.DateController;
import ru.iteco.taskmanager.repository.UUIDController;

public class Project {

	private String uuid;
    private String name;
    private String description;
    private String dateBegin;
    private String dateEnd;
    
    public Project() {
    	
    }
    
    public Project(String _name, String _description) {
    	this.uuid = UUIDController.getUUID();
    	this.name = _name;
    	this.description = _description;
    	this.dateBegin = DateController.getDateBegin();
    	this.dateEnd = DateController.getDateBegin();
    }
    
    public String getUuid() {
		return uuid;
	}

    public String getName() {
        return this.name;
    }

    public void setName(String _name) {
    	this.name = _name;
    }
    
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDateBegin() {
		return dateBegin;
	}

	public String getDateEnd() {
		return dateEnd;
	}

	@Override
	public String toString() {
		return "Name: " + this.name + "\nDescription: " + this.description + "\nDateBegin: " + this.dateBegin + "\nDateEnd: " + this.dateEnd + "\nUUID: " + this.uuid; 
	}
}
