package ru.iteco.taskmanager.entity;

public class Literas {

	public static final String HELP = "help"; 
	public static final String EXIT = "exit";
	
	public static final String NEW_PROJECT = "cp";
	public static final String SHOW_PROJECT = "rp";
	public static final String SHOW_ALL_PROJECT = "allp";
	public static final String UPDATE_PROJECT = "up";
	public static final String DELETE_PROJECT = "dp";
	
	public static final String NEW_TASK = "ct";
	public static final String SHOW_TASK = "rt";
	public static final String SHOW_ALL_TASK = "allt";
	public static final String UPDATE_TASK = "ut";
	public static final String DELETE_TASK = "dt";
}
